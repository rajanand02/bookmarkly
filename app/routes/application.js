import Ember from 'ember';

export default Ember.Route.extend({
  model: function() {
    return Ember.RSVP.hash({
      folders: this.store.findAll('folder'),
      bookmarks: this.store.findAll('bookmark')
    });
  }

});
